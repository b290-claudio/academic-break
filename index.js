/* Javascript - Function Parameters and Return Statement */
/* Javascript - Selection Control Structures */

let shippingFee = 0;

function shipItem(item, weight, location){

	// this is for empty/missing inputs on the parameters
	if (item === "" || item === undefined || 
		weight === "" || weight === undefined || 
		location === "" || location === undefined){
		return `Please input correct shipping details to proceed. No empty fields and lowercase only. (item, weight, location).`
	};

	// this is for weight less than 3 and weight equal to 4
	if (weight < 3 || weight === 4){
		return `This is not included in the offered services please round up the weight by 1.`
	}

	// if conditions to look for correct price for each weight
	if (weight === 3){
		shippingFee = 150;
	};
	if (weight >= 5){
		shippingFee = 280;
	};
	if (weight >= 8){
		shippingFee = 340;
	};
	if (weight >= 10){
		shippingFee = 410;
	};
	if (weight > 10){
		shippingFee = 560;
	};

	// this is for checking if the location input is correct and also add a shipping fee if location input is right
	switch (location){
		case "local":
			totalShippingFee = shippingFee + 0;
			break;
		case "international":
			totalShippingFee = shippingFee + 250;
			break;
		default:
			return `Invalid location, please choose between "local" and "international"`
			break;
	}

	// if all input are correct, this will display.
	return `The total shipping fee for ${item} that will ship ${location} is P${totalShippingFee}.00`
}

// console logging for quick result. User(s) can also invoke on browser console
console.log(`Item to be delivered: Pants | Weight: 5kg | deliver to: local.`)
console.log(shipItem("Pants", 5, "local"));
console.log(`Item to be delivered: Gadgets | Weight: 3kg | deliver to: international.`)
console.log(shipItem("Gadgets", 3, "international"));


/*==================================================================*/

/* JS - Objects */
/* JS - Array Manipulation */

// object constructor for Product
function Product(name, price, quantity){
	this.name = name;
	this.price = price;
	this.quantity = quantity;
};

// object constructor for Customer
function Customer(name, cart){
	this.name = name;
	this.cart = [];

	// function to add item(s) to cart
	this.addToCart = function(items){

		// add the item inside the array cart
		this.cart.push(items);
		items.quantity = 0 + 1;
		return `${items.name} is added to cart.`
	}

	// function to remove item(s) from the cart
	this.removeToCart = function(items){

		// look for index of the item in the array cart
		let n = this.cart.indexOf(items);

		// remove the provided index above from the array cart n=starting index number to splice (.splice(startingIndex, number of items to delete))
		this.cart.splice(n, 1);
		return `${items.name} is removed from the cart.`
	};
};

// instantiate new product
const tShirt = new Product("Bench", 1000, 10);
console.log(tShirt);

// instantiate new product
const shoes = new Product("Sneakers", 1500, 10);
console.log(shoes);

// instantiate new customer
const customer1 = new Customer("Ralph");
console.log(customer1);


// Console logging addToCart and removeToCart for quick result. Users can also invoke this via browser console
console.log(customer1.addToCart(tShirt));

console.log(customer1.addToCart(shoes));

console.log(customer1.removeToCart(shoes));

console.log(customer1.removeToCart(tShirt));

console.log(customer1.cart);